#[derive(Debug, Clone, PartialEq, Copy)]
pub enum PieceColor {
  White,
  Black,
}

#[derive(Debug, Clone, PartialEq, Copy)]
pub struct GamePiece {
  pub color: PieceColor,
  pub crowned: bool,
}

impl GamePiece {
  pub fn new(color: PieceColor) -> GamePiece {
    GamePiece {
      color,
      crowned: false,
    }
  }
  pub fn make_crowned(piece: GamePiece) -> GamePiece {
    GamePiece {
      color: piece.color,
      crowned: true,
    }
  }
}

// Creates Co-ordinates for pieces
#[derive(Debug, Clone, PartialEq, Copy)]
pub struct Coordinate(pub usize, pub usize);

impl Coordinate {
  pub fn check_placement(self) -> bool {
    let Coordinate(x, y) = self;
    x <= 7 && y <= 7
  }

  pub fn jump_targets(&self) -> impl Iterator<Item = Coordinate> {
    let mut pos = Vec::new();
    let Coordinate(x, y) = *self;

    if y >= 2 {
      pos.push(Coordinate(x + 2, y - 2));
    }
    pos.push(Coordinate(x + 2, y + 2));

    if x >= 2 && y >= 2 {
      pos.push(Coordinate(x - 2, y - 2));
    }
    if x >= 2 {
      pos.push(Coordinate(x - 2, y + 2));
    }
    pos.into_iter()
  }

  pub fn move_targets(&self) -> impl Iterator<Item = Coordinate> {
    let mut pos = Vec::new();
    let Coordinate(x, y) = *self;
    if y >= 1 {
      pos.push(Coordinate(x + 1, y - 1));
    }
    pos.push(Coordinate(x + 1, y + 1));

    if x >= 1 && y >= 1 {
      pos.push(Coordinate(x - 1, y - 1));
    }
    if x >= 1 {
      pos.push(Coordinate(x - 1, y + 1));
    }
    pos.into_iter()
  }
}

#[derive(Debug, Clone, PartialEq, Copy)]
pub struct Move{
  pub from: Coordinate,
  pub to: Coordinate,
}

impl Move{
  pub fn new(from: (usize, usize), to: (usize, usize)) -> Move {
    Move {
      from: Coordinate(from.0, from.1),
      to: Coordinate(to.0, to.1),
    }
  }
}