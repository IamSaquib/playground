#include <stdio.h>
#include <stdlib.h>

#define VERSION "1.0.0"

// Multi line #defs 
#define LOG_ERROR(format, ...) \
fprintf(stderr, format, __VA_ARGS__)

int main(int argc, char** argv) {
    printf("%s", VERSION);
    if (argc < 3) {
        LOG_ERROR("Invalid number of Arguments %s\n", VERSION);
        exit(1);
    } else {
        LOG_ERROR("Args are %s %s %s\n", argv[1], argv[2], argv[3]);
    }
    return 0;
}