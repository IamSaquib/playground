#include <time.h>
#include <stdio.h>

int main() {
    time_t timer;
    time(&timer);
    printf("Local Time is %s", ctime(&timer));
    return 0;
}